#+hugo_base_dir: ../
#+hugo_section: ./post

#+seq_todo: TODO DRAFT DONE

#+author: Akshay Gaikwad

* DONE Git diff raw
:PROPERTIES:
:EXPORT_FILE_NAME: git-diff-raw
:EXPORT_DATE: 2019-02-05
:END:

#+begin_src
# git diff --raw <a-blob> <b-blob>
#+end_src

This command provides difference made in =<b-blob>= since
=<a-blob>=. This enlist list of files changed. Also shows fields like
source file mode; destination file mode; its status like 'A' when new
file added, 'D' when file removed, 'M' when file is modified, 'R069'
when file is renamed with 69% of changes; sha1 for sorurce file; sha2
for destination file.

This command is useful to get changelog in between two tags.

References:

1. [[https://git-scm.com/docs/git-diff#_raw_output_format]]

* DONE GTD Workflow by Charles Cave
:PROPERTIES:
:EXPORT_FILE_NAME: gtd-workflow-by-charles-cave
:EXPORT_DATE: 2019-02-09
:END:
Refer [[http://members.optusnet.com.au/~charles57/GTD/gtd_workflow.html][link]] for complete post by Charles.

He has created single org file - newgtd.org to manage all the tasks.
Tasks are classified by Categories and tags.

Heading 1 in the file includes --
1. Tasks   (ToDo list)

   The Remember mode template includes the date added:

   #+begin_src lisp
       ("Todo" ?t "* TODO %^{Brief Description} %^g\n%?\nAdded: %U" 
             "c:/charles/GTD/newgtd.org" "Tasks")
   #+end_src
   
2. Calendar (Holidays, Events, dates)

3. Projects

   #+begin_src lisp
      * Projects
      #+CATEGORY: Projects
      ** Implement Brian Tracy Focal Point Program                     :PROJECT:
      *** Outcome
          DEADLINE: <2009-09-30 Wed>
       Make the Focal Point methodology an ingrained part of my being
      *** TODO Detailed study of Health and Fitness Chapter            :READING:
          DEADLINE: <2009-01-19 Mon>
      *** Detailed study of Business and Career (Focal Point)          :READING:
      *** Detailed study of Family & Personal life (Focal Point)       :READING:
      *** Detailed study of Money and Investments (Focal Point)        :READING:
      *** Detailed study of Personal Growth and Develop (Focal Poin    :READING:
      *** Detailed study of Social and Community (Focal Point)         :READING:
      *** Detailed study of Spiritual Dev & Inner Peace (Focal Point)  :READING:
   #+end_src

4. Financial (Bills to pay, credit cards)

5. Borrowed (Books from Library)

6. Configuration (Emacs org-mode conf of the file)

  #+begin_src lisp
     ** org-mode configuration
     #+STARTUP: overview
     #+STARTUP: hidestars
     #+STARTUP: logdone
     #+PROPERTY: Effort_ALL  0:10 0:20 0:30 1:00 2:00 4:00 6:00 8:00
     #+COLUMNS: %38ITEM(Details) %TAGS(Context) %7TODO(To Do) %5Effort(Time){:} %6CLOCKSUM{Total}
     #+PROPERTY: Effort_ALL 0 0:10 0:20 0:30 1:00 2:00 3:00 4:00 8:00
     #+TAGS: { OFFICE(o) HOME(h) } COMPUTER(c) PROJECT(p) READING(r) 
     #+TAGS: DVD(d) LUNCHTIME(l)
     #+SEQ_TODO: TODO(t) STARTED(s) WAITING(w) APPT(a) | DONE(d) CANCELLED(c) DEFERRED(f)
  #+end_src
  
Tasks are categories as Tasks, Projects under the heading.
Tags like HOME, OFFICE, COMPUTER, READING, LUNCHTIME, PROJECT where LUNCHTIME is for errands he do.

Agenda custom command C-c a H to display tasks at begining of the day.

 #+begin_src lisp
   ("H" "Office and Home Lists"
       ((agenda)
            (tags-todo "OFFICE")
            (tags-todo "HOME")
            (tags-todo "COMPUTER")
            (tags-todo "DVD")
            (tags-todo "READING")))
 #+end_src

This display time specific activities, then tasks scheduled for the day, followed by item for each tag. Next deadline warnings for upcoming days.
Then he schedule tasks hat MUST do today.
Then he create another agenda view which shows just items scheduled for today. This custom command is:

#+begin_src lisp
 ("D" "Daily Action List"
      (
           (agenda "" ((org-agenda-ndays 1)
                       (org-agenda-sorting-strategy
                        (quote ((agenda time-up priority-down tag-up) )))
                       (org-deadline-warning-days 0)
                       ))))
#+end_src

Estimating days work in following durations:

#+begin_src lisp
 #+PROPERTY: Effort_ALL 0 0:10 0:20 0:30 1:00 2:00 4:00 6:00 8:00
#+end_src

Open colums in agenda view by C-c C-x C-c

He review the week using checklist in file [[http://members.optusnet.com.au/~charles57/GTD/weeklyreview.org][weeklyreview.org]]; Also added weekly review task in calendar as repeating task.

Refiling from/to someday.org from/to newgtd.org files by pressing C-c C-w

#+begin_src lisp
 '(org-refile-targets (quote (("newgtd.org" :maxlevel . 1) 
                              ("someday.org" :level . 2))))
#+end_src

Notetaking is in file privnotes.org and journal.org in monthwise sections.

Syncronising is done from differnt machine by pyhon script which copy data to USB drive.

* DONE Stuck in git
:PROPERTIES:
:EXPORT_FILE_NAME: stuck-in-git
:EXPORT_DATE: 2019-02-10
:END:

Many time we see, commits get rejected while pushing to remote, got
error when switching branch, someones' commit appear in our PR. People
stuck at that point and go to look for error on internet.

Here are such scenarios and how to solve it.

*** Unknowingly created commit in master branch

    Ususal method to create a PR is to checkout to new branch, `say
    fix_123` and work on changes in the new branch. But sometimes we
    forgot to create branch and start working on master branch itself.

      #+begin_src shell
      akgaikwad@localhost ~/tmp/demo (master) $ git log
      commit 9d24e323613e4bd950bbd78d935c60ef4f1fca28 (HEAD -> master)
      Author: Akshay Gaikwad <akgaikwad001@gmail.com>
      Date:   Sun Feb 10 19:02:38 2019 +0530

          fixes #123

      commit af9106693fb8966f44b074ee032fdb7b40ed5374
      Author: Akshay Gaikwad <akgaikwad001@gmail.com>
      Date:   Sun Feb 10 18:54:48 2019 +0530

          Initial commit
      #+end_src

    Simply create a new branch from master branch; new commit will be
    there in the branch. and if you don't want commit in the master, execute [[https://git-scm.com/docs/git-reset][git-reset]] as below:

      #+begin_src shell
      akgaikwad@localhost ~/tmp/demo (master) $ git reset --hard HEAD~1
      HEAD is now at af91066 Initial commit
      #+end_src

    Note: If you have new branch already created, then use [[https://git-scm.com/docs/git-cherry-pick][git-cherry-pick]] to
    move commit from master to that branch.

*** Pushing your changes to remote gets rejected.

    Sometimes when you push your changes to the remote, you will see
    it gets rejected with message like this:

      #+begin_src shell
      akgaikwad@localhost ~/tmp/demo (master) $ git push origin master
      To gitlab.com:akshay196/demo.git
       ! [rejected]        master -> master (fetch first)
      error: failed to push some refs to 'git@gitlab.com:akshay196/demo.git'
      hint: Updates were rejected because the remote contains work that you do
      hint: not have locally. This is usually caused by another repository pushing
      hint: to the same ref. You may want to first integrate the remote changes
      hint: (e.g., 'git pull ...') before pushing again.
      hint: See the 'Note about fast-forwards' in 'git push --help' for details.
      #+end_src

    But pull from remote to master gives error as:

      #+begin_src shell
      fatal: refusing to merge unrelated histories
      #+end_src

    This happens when there are changes present at remote branch which are not
    available in local branch. At this time, Run [[https://git-scm.com/docs/git-pull#git-pull---rebasefalsetruemergespreserveinteractive][git-pull with rebase]] enabled.

      #+begin_src shell
      akgaikwad@localhost ~/tmp/demo (master) $ git pull origin master --rebase
      From gitlab.com:akshay196/demo
        branch            master     -> FETCH_HEAD
      First, rewinding head to replay your work on top of it...
      Applying: Initial commit
      #+end_src

* DONE Org agenda appointment notification
:PROPERTIES:
:EXPORT_FILE_NAME: org-agenda-appointment-notification
:EXPORT_DATE: 2019-02-16
:END:
I was looking for the notification method for the tasks which are
schedule and have deadline timing mentioned. I have missed many
scheduled tasks before; Hence need notification.

Emacs library [[https://github.com/p-m/org-notify][org-notify]] and [[https://github.com/spegoraro/org-alert][org-alert]] can use to get alert of
tasks. But for that, GNU/Emacs should run as a daemon. Run GNU/Emacs
daemon as:

#+begin_src shell
  $ emacs --daemon
  or
  $ emacs --fg-daemon
#+end_src

Since org-alert notify every TODO tasks in today's agenda after some
interval of time (default is 5 minutes), I don't like it.

Searching on web get me to the =org-agenda-to-appt= command. This is
very nice feature and it does show me my appointments (tasks that are
scheduled at time) in the buffer above the minibuffer at exact time
when it is scheduled.

I have seen that Emacs shows notification when clocked timing are
running out the Efforts that is set, in the window manager
notification tray. I am looking same for the appointment tasks as well.

There is lisp code mentioned at stackexchange ([[https://emacs.stackexchange.com/a/5821][link]]) to show
appointment notification. This is divided into two parts:

*** Code in Emacs init file

This is lisp code which has custom function for org-agenda-to-appt
(i.e. my-org-agenda-to-appt). It is called by either of three methods.

1. At starting Emacs
2. Everyday at 12:05 am. This is useful in case Emacs is always on.
3. After saving a file. This is after-save-hook that calls
   =my-org-agenda-appt= after file been saved. (I locate my agenda
   file here)

To show it in the window manager notification, it run a shell script.

*** Code in ~/bin/appt-notification

This is bash script which run =notify-send= with parameter as custom
message that includes: $TIME and $MSG from start-process caller in the lisp code.

This method also include warning message before 5 minutes of the
appointment time.
* DONE org-jira mode in Emacs
:PROPERTIES:
:EXPORT_FILE_NAME: org-jira-mode
:EXPORT_DATE: 2019-02-18
:END:
[[https://github.com/ahungry/org-jira][org-jira]] is helpful package when you are using Jira for issue tracking
and loves org-mode. This is easy to manages JIRA issues in org mode.

** Installation
org-jira package is available in MELPA. If you have melpa set up
already then run =M-x package-install RET org-jira RET= to install.

Set jira url as below in the =~/.emacs= file:

#+begin_src lisp
(setq jiralib-url "https://your-site.atlassian.net")
#+end_src

** Usage
Some keybindings are mentioned [[https://github.com/ahungry/org-jira#keybinds][here]]. By default it store all the issue
in the file under =~/.org-jira/= directory. This can be changed using,

#+begin_src lisp
(setq org-jira-working-dir "~/path/to/directory")
#+end_src

It is possible to get issues with [[https://www.atlassian.com/blog/jira-software/jql-the-most-flexible-way-to-search-jira-14][jql query search]] applied.

#+begin_src lisp
(setq org-jira-custom-jqls
 '(
    (:jql " project IN (PROJECT_NAME) and assignee NOT IN ('akshay') and status IN ('New', 'Accepted') "
          :limit 100
          :filename "FILE_NAME")
))
#+end_src

 
Running =M-x org-jira-get-issues-from-custom-jql= will get list of
issues from project =PROJECT_NAME=, assigned to =akshay= and in state =New= or =Accepted=.

This will store list of such issue in file =FILE_NAME= like this:

#+begin_src lisp
    * PROJECT_NAME-Tickets
    ** TODO Title of issue :PROJECT_NAME_3431:
    :PROPERTIES:
    :assignee: akshay
    :filename: PROJECT_NAME_READY
    :reporter: alice
    :type:     Task
    :priority: Normal
    :status:   New
    :components: Some name
    :created:  2019-02-12T07:27:06.000+0000
    :updated:  2019-02-15T07:15:02.000+0000
    :ID:       PROJECT_NAME-3431
    :CUSTOM_ID: PROJECT_NAME-3431
    :END:
    *** description: [PROJECT_NAME-3431]
        This is an issue description.
    *** Comment: Akshay Gaikwad
    :PROPERTIES:
    :ID:       1254167
    :created:  2019-02-12T07:29:13.198+0000
    :END:
       This is the comment line.
#+end_src

Simple way to open issue link in browser is press =C-c ib= or =M-x
org-jira-browse-issue= while curson is on the issue line.

Update issue using =C-c iu= or =M-x org-jira-update-issue=.

Change state of issue using =C-c iw= or =M-x org-jira-progress-issue=.
* DONE Searching in org and agenda mode
:PROPERTIES:
:EXPORT_FILE_NAME: searching-in-org
:EXPORT_DATE: 2019-02-25
:END:
List of command for searching in org file and org agenda mode.

1. =C-c a T=: Search entries with keywords.

   Get a list of items with special keyword.

   =C-c a T=
   =Keyword (or KWD1|K2D2|...): CANCELLED= gives list of CANCELLED tasks

   #+begin_src shell
   Global list of TODO items of type: CANCELLED
   Available with ‘N r’: (0)[ALL] (1)TODO (2)STARTED (3)WAITING (4)APPT (5)DONE (6)CANCELLED (7)DEFERRED
     org:        CANCELLED Taks1 Heading
     org:        CANCELLED Taks2 Heading
   #+end_src

2. =C-c a m=: Search with Tag, Properties

   Match tag, property query.

   =C-c a m=
   =Match: home= list out tasks with :home: tag.

   #+begin_src shell
   Headlines with TAGS match: home
   Press ‘C-u r’ to search again with new search string
     Tasks:      TODO Meet old friends                                                           :home:
   #+end_src

   Specifying property like ~Match: status="Accepted"~ will returns
   tasks which has status property set to Accepted.

   It can be enfold in brackets, which instruct org-mode to treat it
   as a query expression.

   ~Match: status={Progress}~

   #+begin_src shell
   Headlines with TAGS match: status={Progress}
   Press ‘C-u r’ to search again with new search string
     BOARD:   TODO Write blog on searching in org mode
     BOARD:   TODO Task with heading
   #+end_src

   =C-c a M= is like m, but returns only TODO entries as result.

3. =C-c a s=: Serach in file with word/regex

   Search for kwyword in org file.

   =C-c a s=
   =Phrase or [+-]Word/{Regexp} ...: work=

   #+begin_src shell
   Search words: work
   Press ‘[’, ‘]’ to add/sub word, ‘{’, ‘}’ to add/sub regexp, ‘C-u r’ to edit
     org:        DONE GTD workflow
     org:        GTD link
   #+end_src

   =C-c a S= is similar to =s= but returns only entries.

 For more detailed information about advance searching in org, read
   [[https://orgmode.org/worg/org-tutorials/advanced-searching.html][advance searching tutorial in org-mode site]].


** Search with scheduled and deadline dates

1. =C-c / d=: Check Org deadline.

   Highlight deadline entries within =org-deadline-warning-days=. [fn:1]

   =C-7 C-c / d= shows all deadlines due 7 days.

2. =C-c / b=: Check deadline/schedule before date.

   Check deadline before date inputted.

   =C-c / b= =2019-02-27= displays deadline/schedules tasks before
   2019-02-27 date.

   #+begin_src shell
   * Tasks [2/10]                                                     :PERSONAL:
     ** TODO Read book
        SCHEDULED: <2019-02-25 Mon>
        :PROPERTIES:...
     ** TODO Typing practice
     SCHEDULED: <2019-02-26 Tue>
     :PROPERTIES:...
   ...
     ** DONE Read blog post
     DEADLINE: <2019-02-22 Fri>
   #+end_src

3. =C-c / a=: Check deadline/schedule after date.

   Sparse tree for deadlines and schedules items after a given date.

Read [[https://orgmode.org/manual/Inserting-deadline_002fschedule.html][inserting deadline and schedule manual page]] on orgmode.


[fn:1] =org-deadline-warings-days= can use to specify days counts
past-due to show waring of deadline in agenda view.

=(setq org-deadline-warings-days 3)= shows deadline within 3 days.
* DONE Python debugger commands
:PROPERTIES:
:EXPORT_FILE_NAME: debugger-commands
:EXPORT_DATE: 2019-04-12
:END:
The pdb module use to debug code in Python. Add breakpoint in the code as:

#+begin_src python
import pdb; pdb.set_trace()
#+end_src

Few commands recognized by the debugger are listed below:

*h* : Help

*w* : where. print stack trace with most recent frame at bottom

*d* and *u* : Down and Up the current frame

*s* : Step. Execute current line stop at next function called or next line.

*n* : next. Next line

*unt* [lineno] : Until line number, execute

*r* : Return. Continue until current funtion returns

*l* : list.

*ll* : long list.

*display* [expression] : Display value of an expression if it
changesd each time execution stop in the current frame.

*undisplay* [expression] : Do not display.

*run* or *restart* : Restart debugging.

For more information visit [[https://docs.python.org/3/library/pdb.html#debugger-commands][Python pdb library]].
* DONE Inherit tags when archiving subtree in Org
:PROPERTIES:
:EXPORT_FILE_NAME: inherit-tags-when-archiving-subtree-in-org
:EXPORT_DATE: 2019-04-21
:END:
=org-archive-subtree-add-inherited-tags= is a variable defined in ~org-archive.el~.

This variable appends inherited tags when archiving a subtree. Original value is =infile=.

It is now set to =t= (always) which appends tags from parent of the tree to archived tree.

#+begin_src lisp
After archiving "Sub Heading":

 * Heading            :headtag:    =>   * Sub heading       :headtag:subtag:
 ** Sub Heading       :subtag:
#+end_src

Reference:
   [[https://lists.gnu.org/archive/html/emacs-orgmode/2011-02/msg00036.html][Mail by Carsten Dominik for implemetation of org-archive-subtree-add-inherited-tags]]
* DONE GNU Emacs Elpy mode commands
:PROPERTIES:
:EXPORT_FILE_NAME: emacs-elpy-mode-commands
:EXPORT_DATE: 2019-04-27
:END:
Activate Python virtual environment:
   =M-x pyvenv-activate=

Elpy mode commands:

- Navaigation shortcuts:

    |-----------+------------------------------------------------|
    |           |                                                |
    |-----------+------------------------------------------------|
    | =M-.=     | Go to definition                               |
    | =M-,=     | Return to place where =M-.= used. (Alt: =M-*=) |
    | =C-x 4 .= | Open definition in other window                |
    | =C-x 5 .= | Open definition in other frame                 |
    | =C-c C-o= | Overview class/functions in current buffer     |
    | =C-c C-j= | imenu: Jump to definition directly             |
    | =M-?=     | Open document for python symbols               |
    | =C-c C-c= | Execute code in the buffer                     |
    |-----------+------------------------------------------------|


- Python shell
    |-----------+-------------------------|
    |           |                         |
    |-----------+-------------------------|
    | =C-c C-z= | Open Python interpreter |
    | =C-c C-k= | Kill Python shell       |
    |-----------+-------------------------|


- Evaluate code fragment:

    |-------------+------------------------------------------------------------------------------|
    |             |                                                                              |
    |-------------+------------------------------------------------------------------------------|
    | =C-c C-y e= | Send current line/function/region to Python shell. Displays output in buffer |
    |-------------+------------------------------------------------------------------------------|
      For other code evaluation command visit [[https://elpy.readthedocs.io/en/latest/ide.html#evaluating-code-fragments][link]]

- Syntax check

    |-----------+----------------------------------------------------------------------------------|
    |           |                                                                                  |
    |-----------+----------------------------------------------------------------------------------|
    | =C-c C-v= | Elpy chec for errors. also can jump to error line from overview of errors buffer |
    | =C-c C-n= | Flymake next error                                                               |
    | =C-c C-p= | Flymake previous error                                                           |
    |-----------+----------------------------------------------------------------------------------|


- Testing

    |----------------------------+----------------------------------------------------|
    |                            |                                                    |
    |----------------------------+----------------------------------------------------|
    | =C-c C-t=                  | Run test                                           |
    | =M-x elpy-set-test-runner= | Change test runners: pytest, unittest, Django etc. |
    |----------------------------+----------------------------------------------------|


- Refactoring

    |-------------+-----------------------------------------------------------------------------------------|
    |             |                                                                                         |
    |-------------+-----------------------------------------------------------------------------------------|
    | =C-c c-e=   | Multiedit python symbols                                                                |
    | =C-c C-r f= | Format code (black, autopep8, yapf). If region selected, only that region is formatted. |
    | =C-c C-r r= | Run refactoring. use [[https://github.com/python-rope/rope][rope]] package                                                       |
    |-------------+-----------------------------------------------------------------------------------------|

     For more details, [[https://elpy.readthedocs.io/en/latest/][visit elpy document]].
* DONE Build GNU Emacs from source code on Fedora
:PROPERTIES:
:EXPORT_FILE_NAME: emacs-from-source-code
:EXPORT_DATE: 2019-05-04
:END:
This is my first attempt to install GNU Emacs from source code. After
resolving many errors, these are step to compile GNU Emacs.

1. Download Emacs source code.

   a. Source code can be download from
   https://ftp.gnu.org/pub/gnu/emacs in tar file. Then extract source file.

   b. Alternatively, Clone code using git.

       #+BEGIN_SRC shell
        $ git clone git://git.savannah.gnu.org/emacs.git
        $ cd emacs
       #+END_SRC

2. If source code is downloaded using git. Then install =autoconf=
   package and execute =./autogen.sh= in emacs current directory.

  #+BEGIN_SRC shell
  [akshay@fedora30 emacs]$ ./autogen.sh
  Checking whether you have the necessary tools...
  (Read INSTALL.REPO for more details on building Emacs)
  Checking for autoconf (need at least version 2.65) ... ok
  Your system has the required tools.
  Building aclocal.m4 ...
  Running 'autoreconf -fi -I m4' ...
  Configuring local git repository...
  '.git/config' -> '.git/config.~1~'
  git config transfer.fsckObjects 'true'
  git config diff.elisp.xfuncname '^\(def[^[:space:]]+[[:space:]]+([^()[:space:]]+)'
  git config diff.m4.xfuncname '^((m4_)?define|A._DEFUN(_ONCE)?)\([^),]*'
  git config diff.make.xfuncname '^([$.[:alnum:]_].*:|[[:alnum:]_]+[[:space:]]*([*:+]?[:?]?|!?)=|define .*)'
  git config diff.shell.xfuncname '^([[:space:]]*[[:alpha:]_][[:alnum:]_]*[[:space:]]*\(\)|[[:alpha:]_][[:alnum:]_]*=)'
  git config diff.texinfo.xfuncname '^@node[[:space:]]+([^,[:space:]][^,]+)'
  Installing git hooks...
  'build-aux/git-hooks/commit-msg' -> '.git/hooks/commit-msg'
  'build-aux/git-hooks/pre-commit' -> '.git/hooks/pre-commit'
  '.git/hooks/applypatch-msg.sample' -> '.git/hooks/applypatch-msg'
  '.git/hooks/pre-applypatch.sample' -> '.git/hooks/pre-applypatch'
  You can now run './configure'.
  #+END_SRC

3. Install all required dependencies for emacs.

  #+BEGIN_SRC shell
   $ sudo dnf builddep emacs
  #+END_SRC

  Some dependencies I have noticed and need to installed if not
   installed. These are,

   - giflib-devel
   - libXpm-devel
   - libjpeg-devel
   - gnutls-devel
   - gtk3-devel
   - texinfo  (provides =makeinfo=)

4. Run =configure= script.

   #+BEGIN_SRC shell
     $ ./configure
   #+END_SRC

   Incase if following error occurs:

      #+BEGIN_SRC shell
      config.status: executing etc-refcards-emacsver.tex commands
      configure: WARNING: This configuration installs a 'movemail' program
      that does not retrieve POP3 email.  By default, Emacs 25 and earlier
      installed a 'movemail' program that retrieved POP3 email via only
      insecure channels, a practice that is no longer recommended but that
      you can continue to support by using './configure --with-pop'.
      configure: You might want to install GNU Mailutils
      <https://mailutils.org> and use './configure --with-mailutils'.
      #+END_SRC

   You need to install =mailx= package on Feodra and
   [[http://mailutils.org/][mailutils]] on Ubuntu. Then use,

     #+BEGIN_SRC shell
      $ ./configure --with-mailutils
     #+END_SRC

5. Invoke 'make'

     #+BEGIN_SRC shell
       $ make
     #+END_SRC

   If =make= succeeds, it will build an executable program 'emacs' in
   'src' directory.

6. Start Emacs

   #+BEGIN_SRC shell
    $ cd src
    $ ./emacs -Q
   #+END_SRC
* DONE Emacs Diary Mode
:PROPERTIES:
:EXPORT_FILE_NAME: emacs-diary-mode
:EXPORT_DATE: 2019-05-26
:END:
=~/.emacs.d/diary= is Emacs default diary file. The name of diary file
is specified by the variable =diary-file=.

Some formats of diary file from [[https://www.gnu.org/software/emacs/manual/html_node/emacs/Format-of-Diary-File.html#Format-of-Diary-File][link]]:

#+BEGIN_SRC org
     12/22/2015  Twentieth wedding anniversary!
     10/22       Ruth's birthday.
     * 21, *:    Payday
     Tuesday--weekly meeting with grad students at 10am
              Supowit, Shen, Bitner, and Kapoor to attend.
     1/13/89     Friday the thirteenth!!
     thu 4pm     squash game with Lloyd.
     mar 16      Dad's birthday
     April 15, 2016 Income tax due.
     * 15        time cards due.

     02/11/2012
           Bill B. visits Princeton today
           2pm Cognitive Studies Committee meeting
           2:30-5:30 Liz at Lawrenceville
           4:00pm Dentist appt
           7:30pm Dinner at George's
           8:00-10:00pm concert
#+END_SRC

Use these command in Calendar Mode,

 =d=    : Display all diary entries for the selected date.

 =s=    : show all entries of diary file

 =m=    : Mark dates with diary entries

 =x=    : Visisble all holidays

 =c=    : Go to agenda buffer for the date

 =u=    : Unmark entries

 =M-x diary-mail-entries=  : Mail diary entries for today's date

 =M-x diary-print-entries= : Print hard copy of diary


Few other necessary commands:

 =M-x holidays or list-holidays=  : Displays holidays

 =S=  : Display times of sunrise and sunset for the selected date

 =M=  : Display the dates and times for all the quarters of the moon

Press =p= in calendar buffer.
* DONE Emacs new commands
:PROPERTIES:
:EXPORT_FILE_NAME: emacs-new-commands
:EXPORT_DATE: 2019-06-29
:END:
I have started reading "Learning GNU Emacs" book and completed first 5
chapters. The books is too useful to know about all the commands
required for a regular Emacs user. Few newer commands I learned from
this book are mentioned below.

C-x C-v => Find alternative file if you open wrong file.

C-x i => Insert content of another file into current buffer at the
position of the pointer.

C-x C-w => Save as file.

M-n command => Perform command n times.

C-s C-w => Serach word.

C-s C-y => Search using kill ring (Press M-p to go to previous item in kill ring)

M-% => Query replace

C-M-s => RE search

C-x C-q => Make buffer read only (type again to remove read-only mode)

C-x 4 f/b/r => Open new window using file/buffer/read-only file. (To open new frame press C-x 5 f/b/r)

C-x 5 2 => Open new frame

C-x 5 0 => Close frame

M-| => Run shell command on selected region in the buffer.

C-c C-o => Flush output of previous shell command.

C-c C-r => Move pointer to the start of last shell command.

C-c C-e => Move to last line of the input to the bottom of the window.

C-c C-p and C-n c-n => Move pointer up/down by shell command output.

~ => Mark backup files in dired mode (*~)

'#' => Mark auto-save files in dired mode (#*)

%m => use regex to mark files in dired mode

i => insert subdirectory in same buffer.

C-u k => Remove subdirectory.

$ => Hide/unhide subdirectory in dired mode.

M-$ => hide/unhide all the sundirectories in dired mode.
