+++
title = "Python debugger commands"
author = ["Akshay Gaikwad"]
date = 2019-04-12
draft = false
+++

The pdb module use to debug code in Python. Add breakpoint in the code as:

```python
import pdb; pdb.set_trace()
```

Few commands recognized by the debugger are listed below:

**h** : Help

**w** : where. print stack trace with most recent frame at bottom

**d** and **u** : Down and Up the current frame

**s** : Step. Execute current line stop at next function called or next line.

**n** : next. Next line

**unt** [lineno] : Until line number, execute

**r** : Return. Continue until current funtion returns

**l** : list.

**ll** : long list.

**display** [expression] : Display value of an expression if it
changesd each time execution stop in the current frame.

**undisplay** [expression] : Do not display.

**run** or **restart** : Restart debugging.

For more information visit [Python pdb library](https://docs.python.org/3/library/pdb.html#debugger-commands).