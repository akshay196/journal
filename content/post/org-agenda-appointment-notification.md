+++
title = "Org agenda appointment notification"
author = ["Akshay Gaikwad"]
date = 2019-02-16
draft = false
+++

I was looking for the notification method for the tasks which are
schedule and have deadline timing mentioned. I have missed many
scheduled tasks before; Hence need notification.

Emacs library [org-notify](https://github.com/p-m/org-notify) and [org-alert](https://github.com/spegoraro/org-alert) can use to get alert of
tasks. But for that, GNU/Emacs should run as a daemon. Run GNU/Emacs
daemon as:

```shell
$ emacs --daemon
or
$ emacs --fg-daemon
```

Since org-alert notify every TODO tasks in today's agenda after some
interval of time (default is 5 minutes), I don't like it.

Searching on web get me to the `org-agenda-to-appt` command. This is
very nice feature and it does show me my appointments (tasks that are
scheduled at time) in the buffer above the minibuffer at exact time
when it is scheduled.

I have seen that Emacs shows notification when clocked timing are
running out the Efforts that is set, in the window manager
notification tray. I am looking same for the appointment tasks as well.

There is lisp code mentioned at stackexchange ([link](https://emacs.stackexchange.com/a/5821)) to show
appointment notification. This is divided into two parts:


## Code in Emacs init file {#code-in-emacs-init-file}

This is lisp code which has custom function for org-agenda-to-appt
(i.e. my-org-agenda-to-appt). It is called by either of three methods.

1.  At starting Emacs
2.  Everyday at 12:05 am. This is useful in case Emacs is always on.
3.  After saving a file. This is after-save-hook that calls
    `my-org-agenda-appt` after file been saved. (I locate my agenda
    file here)

To show it in the window manager notification, it run a shell script.


## Code in ~/bin/appt-notification {#code-in-bin-appt-notification}

This is bash script which run `notify-send` with parameter as custom
message that includes: $TIME and $MSG from start-process caller in the lisp code.

This method also include warning message before 5 minutes of the
appointment time.