+++
title = "Stuck in git"
author = ["Akshay Gaikwad"]
date = 2019-02-10
draft = false
+++

Many time we see, commits get rejected while pushing to remote, got
error when switching branch, someones' commit appear in our PR. People
stuck at that point and go to look for error on internet.

Here are such scenarios and how to solve it.


## Unknowingly created commit in master branch {#unknowingly-created-commit-in-master-branch}

Ususal method to create a PR is to checkout to new branch, \`say
fix\_123\` and work on changes in the new branch. But sometimes we
forgot to create branch and start working on master branch itself.

```shell
akgaikwad@localhost ~/tmp/demo (master) $ git log
commit 9d24e323613e4bd950bbd78d935c60ef4f1fca28 (HEAD -> master)
Author: Akshay Gaikwad <akgaikwad001@gmail.com>
Date:   Sun Feb 10 19:02:38 2019 +0530

    fixes #123

commit af9106693fb8966f44b074ee032fdb7b40ed5374
Author: Akshay Gaikwad <akgaikwad001@gmail.com>
Date:   Sun Feb 10 18:54:48 2019 +0530

    Initial commit
```

Simply create a new branch from master branch; new commit will be
there in the branch. and if you don't want commit in the master, execute [git-reset](https://git-scm.com/docs/git-reset) as below:

```shell
akgaikwad@localhost ~/tmp/demo (master) $ git reset --hard HEAD~1
HEAD is now at af91066 Initial commit
```

Note: If you have new branch already created, then use [git-cherry-pick](https://git-scm.com/docs/git-cherry-pick) to
move commit from master to that branch.


## Pushing your changes to remote gets rejected. {#pushing-your-changes-to-remote-gets-rejected-dot}

Sometimes when you push your changes to the remote, you will see
it gets rejected with message like this:

```shell
akgaikwad@localhost ~/tmp/demo (master) $ git push origin master
To gitlab.com:akshay196/demo.git
 ! [rejected]        master -> master (fetch first)
error: failed to push some refs to 'git@gitlab.com:akshay196/demo.git'
hint: Updates were rejected because the remote contains work that you do
hint: not have locally. This is usually caused by another repository pushing
hint: to the same ref. You may want to first integrate the remote changes
hint: (e.g., 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
```

But pull from remote to master gives error as:

```shell
fatal: refusing to merge unrelated histories
```

This happens when there are changes present at remote branch which are not
available in local branch. At this time, Run [git-pull with rebase](https://git-scm.com/docs/git-pull#git-pull---rebasefalsetruemergespreserveinteractive) enabled.

```shell
akgaikwad@localhost ~/tmp/demo (master) $ git pull origin master --rebase
From gitlab.com:akshay196/demo
  branch            master     -> FETCH_HEAD
First, rewinding head to replay your work on top of it...
Applying: Initial commit
```