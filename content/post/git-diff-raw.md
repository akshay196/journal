+++
title = "Git diff raw"
author = ["Akshay Gaikwad"]
date = 2019-02-05
draft = false
+++

```nil
# git diff --raw <a-blob> <b-blob>
```

This command provides difference made in `<b-blob>` since
`<a-blob>`. This enlist list of files changed. Also shows fields like
source file mode; destination file mode; its status like 'A' when new
file added, 'D' when file removed, 'M' when file is modified, 'R069'
when file is renamed with 69% of changes; sha1 for sorurce file; sha2
for destination file.

This command is useful to get changelog in between two tags.

References:

1.  <https://git-scm.com/docs/git-diff#_raw_output_format>