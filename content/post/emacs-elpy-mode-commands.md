+++
title = "GNU Emacs Elpy mode commands"
author = ["Akshay Gaikwad"]
date = 2019-04-27
draft = false
+++

Activate Python virtual environment:
   `M-x pyvenv-activate`

Elpy mode commands:

-   Navaigation shortcuts:

    |           |                                                |
    |-----------|------------------------------------------------|
    | `M-.`     | Go to definition                               |
    | `M-,`     | Return to place where `M-.` used. (Alt: `M-*`) |
    | `C-x 4 .` | Open definition in other window                |
    | `C-x 5 .` | Open definition in other frame                 |
    | `C-c C-o` | Overview class/functions in current buffer     |
    | `C-c C-j` | imenu: Jump to definition directly             |
    | `M-?`     | Open document for python symbols               |
    | `C-c C-c` | Execute code in the buffer                     |

<!--listend-->

-   Python shell

    |           |                         |
    |-----------|-------------------------|
    | `C-c C-z` | Open Python interpreter |
    | `C-c C-k` | Kill Python shell       |

<!--listend-->

-   Evaluate code fragment:

    |             |                                                                              |
    |-------------|------------------------------------------------------------------------------|
    | `C-c C-y e` | Send current line/function/region to Python shell. Displays output in buffer |

    For other code evaluation command visit [link](https://elpy.readthedocs.io/en/latest/ide.html#evaluating-code-fragments)

-   Syntax check

    |           |                                                                                  |
    |-----------|----------------------------------------------------------------------------------|
    | `C-c C-v` | Elpy chec for errors. also can jump to error line from overview of errors buffer |
    | `C-c C-n` | Flymake next error                                                               |
    | `C-c C-p` | Flymake previous error                                                           |

<!--listend-->

-   Testing

    |                            |                                                    |
    |----------------------------|----------------------------------------------------|
    | `C-c C-t`                  | Run test                                           |
    | `M-x elpy-set-test-runner` | Change test runners: pytest, unittest, Django etc. |

<!--listend-->

-   Refactoring

    |             |                                                                                         |
    |-------------|-----------------------------------------------------------------------------------------|
    | `C-c c-e`   | Multiedit python symbols                                                                |
    | `C-c C-r f` | Format code (black, autopep8, yapf). If region selected, only that region is formatted. |
    | `C-c C-r r` | Run refactoring. use [rope](https://github.com/python-rope/rope) package                |

    For more details, [visit elpy document](https://elpy.readthedocs.io/en/latest/).