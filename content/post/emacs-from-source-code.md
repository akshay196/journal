+++
title = "Build GNU Emacs from source code on Fedora"
author = ["Akshay Gaikwad"]
date = 2019-05-04
draft = false
+++

This is my first attempt to install GNU Emacs from source code. After
resolving many errors, these are step to compile GNU Emacs.

1.  Download Emacs source code.

    a. Source code can be download from
    <https://ftp.gnu.org/pub/gnu/emacs> in tar file. Then extract source file.

    b. Alternatively, Clone code using git.

    ```shell
    $ git clone git://git.savannah.gnu.org/emacs.git
    $ cd emacs
    ```

2.  If source code is downloaded using git. Then install `autoconf`
    package and execute `./autogen.sh` in emacs current directory.

    ```shell
    [akshay@fedora30 emacs]$ ./autogen.sh
    Checking whether you have the necessary tools...
    (Read INSTALL.REPO for more details on building Emacs)
    Checking for autoconf (need at least version 2.65) ... ok
    Your system has the required tools.
    Building aclocal.m4 ...
    Running 'autoreconf -fi -I m4' ...
    Configuring local git repository...
    '.git/config' -> '.git/config.~1~'
    git config transfer.fsckObjects 'true'
    git config diff.elisp.xfuncname '^\(def[^[:space:]]+[[:space:]]+([^()[:space:]]+)'
    git config diff.m4.xfuncname '^((m4_)?define|A._DEFUN(_ONCE)?)\([^),]*'
    git config diff.make.xfuncname '^([$.[:alnum:]_].*:|[[:alnum:]_]+[[:space:]]*([*:+]?[:?]?|!?)=|define .*)'
    git config diff.shell.xfuncname '^([[:space:]]*[[:alpha:]_][[:alnum:]_]*[[:space:]]*\(\)|[[:alpha:]_][[:alnum:]_]*=)'
    git config diff.texinfo.xfuncname '^@node[[:space:]]+([^,[:space:]][^,]+)'
    Installing git hooks...
    'build-aux/git-hooks/commit-msg' -> '.git/hooks/commit-msg'
    'build-aux/git-hooks/pre-commit' -> '.git/hooks/pre-commit'
    '.git/hooks/applypatch-msg.sample' -> '.git/hooks/applypatch-msg'
    '.git/hooks/pre-applypatch.sample' -> '.git/hooks/pre-applypatch'
    You can now run './configure'.
    ```

3.  Install all required dependencies for emacs.

    ```shell
    $ sudo dnf builddep emacs
    ```

    Some dependencies I have noticed and need to installed if not
     installed. These are,

    -   giflib-devel
    -   libXpm-devel
    -   libjpeg-devel
    -   gnutls-devel
    -   gtk3-devel
    -   texinfo  (provides `makeinfo`)

4.  Run `configure` script.

    ```shell
    $ ./configure
    ```

    Incase if following error occurs:

    ```shell
    config.status: executing etc-refcards-emacsver.tex commands
    configure: WARNING: This configuration installs a 'movemail' program
    that does not retrieve POP3 email.  By default, Emacs 25 and earlier
    installed a 'movemail' program that retrieved POP3 email via only
    insecure channels, a practice that is no longer recommended but that
    you can continue to support by using './configure --with-pop'.
    configure: You might want to install GNU Mailutils
    <https://mailutils.org> and use './configure --with-mailutils'.
    ```

    You need to install `mailx` package on Feodra and
    [mailutils](http://mailutils.org/) on Ubuntu. Then use,

    ```shell
    $ ./configure --with-mailutils
    ```

5.  Invoke 'make'

    ```shell
    $ make
    ```

    If `make` succeeds, it will build an executable program 'emacs' in
    'src' directory.

6.  Start Emacs

    ```shell
    $ cd src
    $ ./emacs -Q
    ```