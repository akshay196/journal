+++
title = "org-jira mode in Emacs"
author = ["Akshay Gaikwad"]
date = 2019-02-18
draft = false
+++

[org-jira](https://github.com/ahungry/org-jira) is helpful package when you are using Jira for issue tracking
and loves org-mode. This is easy to manages JIRA issues in org mode.


## Installation {#installation}

org-jira package is available in MELPA. If you have melpa set up
already then run `M-x package-install RET org-jira RET` to install.

Set jira url as below in the `~/.emacs` file:

```lisp
(setq jiralib-url "https://your-site.atlassian.net")
```


## Usage {#usage}

Some keybindings are mentioned [here](https://github.com/ahungry/org-jira#keybinds). By default it store all the issue
in the file under `~/.org-jira/` directory. This can be changed using,

```lisp
(setq org-jira-working-dir "~/path/to/directory")
```

It is possible to get issues with [jql query search](https://www.atlassian.com/blog/jira-software/jql-the-most-flexible-way-to-search-jira-14) applied.

```lisp
(setq org-jira-custom-jqls
 '(
    (:jql " project IN (PROJECT_NAME) and assignee NOT IN ('akshay') and status IN ('New', 'Accepted') "
          :limit 100
          :filename "FILE_NAME")
))
```

Running `M-x org-jira-get-issues-from-custom-jql` will get list of
issues from project `PROJECT_NAME`, assigned to `akshay` and in state `New` or `Accepted`.

This will store list of such issue in file `FILE_NAME` like this:

```lisp
* PROJECT_NAME-Tickets
** TODO Title of issue :PROJECT_NAME_3431:
:PROPERTIES:
:assignee: akshay
:filename: PROJECT_NAME_READY
:reporter: alice
:type:     Task
:priority: Normal
:status:   New
:components: Some name
:created:  2019-02-12T07:27:06.000+0000
:updated:  2019-02-15T07:15:02.000+0000
:ID:       PROJECT_NAME-3431
:CUSTOM_ID: PROJECT_NAME-3431
:END:
*** description: [PROJECT_NAME-3431]
    This is an issue description.
*** Comment: Akshay Gaikwad
:PROPERTIES:
:ID:       1254167
:created:  2019-02-12T07:29:13.198+0000
:END:
   This is the comment line.
```

Simple way to open issue link in browser is press `C-c ib` or `M-x
org-jira-browse-issue` while curson is on the issue line.

Update issue using `C-c iu` or `M-x org-jira-update-issue`.

Change state of issue using `C-c iw` or `M-x org-jira-progress-issue`.