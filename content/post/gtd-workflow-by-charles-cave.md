+++
title = "GTD Workflow by Charles Cave"
author = ["Akshay Gaikwad"]
date = 2019-02-09
draft = false
+++

Refer [link](http://members.optusnet.com.au/~charles57/GTD/gtd_workflow.html) for complete post by Charles.

He has created single org file - newgtd.org to manage all the tasks.
Tasks are classified by Categories and tags.

Heading 1 in the file includes --

1.  Tasks   (ToDo list)

    The Remember mode template includes the date added:

    ```lisp
    ("Todo" ?t "* TODO %^{Brief Description} %^g\n%?\nAdded: %U"
          "c:/charles/GTD/newgtd.org" "Tasks")
    ```

2.  Calendar (Holidays, Events, dates)

3.  Projects

    ```lisp
    ​* Projects
    #+CATEGORY: Projects
    ** Implement Brian Tracy Focal Point Program                     :PROJECT:
    *** Outcome
        DEADLINE: <2009-09-30 Wed>
     Make the Focal Point methodology an ingrained part of my being
    *** TODO Detailed study of Health and Fitness Chapter            :READING:
        DEADLINE: <2009-01-19 Mon>
    *** Detailed study of Business and Career (Focal Point)          :READING:
    *** Detailed study of Family & Personal life (Focal Point)       :READING:
    *** Detailed study of Money and Investments (Focal Point)        :READING:
    *** Detailed study of Personal Growth and Develop (Focal Poin    :READING:
    *** Detailed study of Social and Community (Focal Point)         :READING:
    *** Detailed study of Spiritual Dev & Inner Peace (Focal Point)  :READING:
    ```

4.  Financial (Bills to pay, credit cards)

5.  Borrowed (Books from Library)

6.  Configuration (Emacs org-mode conf of the file)

    ```lisp
    ** org-mode configuration
    #+STARTUP: overview
    #+STARTUP: hidestars
    #+STARTUP: logdone
    #+PROPERTY: Effort_ALL  0:10 0:20 0:30 1:00 2:00 4:00 6:00 8:00
    #+COLUMNS: %38ITEM(Details) %TAGS(Context) %7TODO(To Do) %5Effort(Time){:} %6CLOCKSUM{Total}
    #+PROPERTY: Effort_ALL 0 0:10 0:20 0:30 1:00 2:00 3:00 4:00 8:00
    #+TAGS: { OFFICE(o) HOME(h) } COMPUTER(c) PROJECT(p) READING(r)
    #+TAGS: DVD(d) LUNCHTIME(l)
    #+SEQ_TODO: TODO(t) STARTED(s) WAITING(w) APPT(a) | DONE(d) CANCELLED(c) DEFERRED(f)
    ```

Tasks are categories as Tasks, Projects under the heading.
Tags like HOME, OFFICE, COMPUTER, READING, LUNCHTIME, PROJECT where LUNCHTIME is for errands he do.

Agenda custom command C-c a H to display tasks at begining of the day.

```lisp
("H" "Office and Home Lists"
    ((agenda)
	 (tags-todo "OFFICE")
	 (tags-todo "HOME")
	 (tags-todo "COMPUTER")
	 (tags-todo "DVD")
	 (tags-todo "READING")))
```

This display time specific activities, then tasks scheduled for the day, followed by item for each tag. Next deadline warnings for upcoming days.
Then he schedule tasks hat MUST do today.
Then he create another agenda view which shows just items scheduled for today. This custom command is:

```lisp
("D" "Daily Action List"
     (
	  (agenda "" ((org-agenda-ndays 1)
		      (org-agenda-sorting-strategy
		       (quote ((agenda time-up priority-down tag-up) )))
		      (org-deadline-warning-days 0)
		      ))))
```

Estimating days work in following durations:

```lisp
#+PROPERTY: Effort_ALL 0 0:10 0:20 0:30 1:00 2:00 4:00 6:00 8:00
```

Open colums in agenda view by C-c C-x C-c

He review the week using checklist in file [weeklyreview.org](http://members.optusnet.com.au/~charles57/GTD/weeklyreview.org); Also added weekly review task in calendar as repeating task.

Refiling from/to someday.org from/to newgtd.org files by pressing C-c C-w

```lisp
'(org-refile-targets (quote (("newgtd.org" :maxlevel . 1)
			     ("someday.org" :level . 2))))
```

Notetaking is in file privnotes.org and journal.org in monthwise sections.

Syncronising is done from differnt machine by pyhon script which copy data to USB drive.