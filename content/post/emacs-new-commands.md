+++
title = "Emacs new commands"
author = ["Akshay Gaikwad"]
date = 2019-06-29
draft = false
+++

I have started reading "Learning GNU Emacs" book and completed first 5
chapters. The books is too useful to know about all the commands
required for a regular Emacs user. Few newer commands I learned from
this book are mentioned below.

C-x C-v => Find alternative file if you open wrong file.

C-x i => Insert content of another file into current buffer at the
position of the pointer.

C-x C-w => Save as file.

M-n command => Perform command n times.

C-s C-w => Serach word.

C-s C-y => Search using kill ring (Press M-p to go to previous item in kill ring)

M-% => Query replace

C-M-s => RE search

C-x C-q => Make buffer read only (type again to remove read-only mode)

C-x 4 f/b/r => Open new window using file/buffer/read-only file. (To open new frame press C-x 5 f/b/r)

C-x 5 2 => Open new frame

C-x 5 0 => Close frame

M-| => Run shell command on selected region in the buffer.

C-c C-o => Flush output of previous shell command.

C-c C-r => Move pointer to the start of last shell command.

C-c C-e => Move to last line of the input to the bottom of the window.

C-c C-p and C-n c-n => Move pointer up/down by shell command output.

~ => Mark backup files in dired mode (\*~)

'#' => Mark auto-save files in dired mode (#\*)

%m => use regex to mark files in dired mode

i => insert subdirectory in same buffer.

C-u k => Remove subdirectory.

$ => Hide/unhide subdirectory in dired mode.

M-$ => hide/unhide all the sundirectories in dired mode.
