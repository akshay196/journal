+++
title = "Emacs Diary Mode"
author = ["Akshay Gaikwad"]
date = 2019-05-26
draft = false
+++

`~/.emacs.d/diary` is Emacs default diary file. The name of diary file
is specified by the variable `diary-file`.

Some formats of diary file from [link](https://www.gnu.org/software/emacs/manual/html_node/emacs/Format-of-Diary-File.html#Format-of-Diary-File):

```org
12/22/2015  Twentieth wedding anniversary!
10/22       Ruth's birthday.
* 21, *:    Payday
Tuesday--weekly meeting with grad students at 10am
         Supowit, Shen, Bitner, and Kapoor to attend.
1/13/89     Friday the thirteenth!!
thu 4pm     squash game with Lloyd.
mar 16      Dad's birthday
April 15, 2016 Income tax due.
* 15        time cards due.

02/11/2012
      Bill B. visits Princeton today
      2pm Cognitive Studies Committee meeting
      2:30-5:30 Liz at Lawrenceville
      4:00pm Dentist appt
      7:30pm Dinner at George's
      8:00-10:00pm concert
```

Use these command in Calendar Mode,

`d`    : Display all diary entries for the selected date.

`s`    : show all entries of diary file

`m`    : Mark dates with diary entries

`x`    : Visisble all holidays

`c`    : Go to agenda buffer for the date

`u`    : Unmark entries

`M-x diary-mail-entries`  : Mail diary entries for today's date

`M-x diary-print-entries` : Print hard copy of diary

Few other necessary commands:

`M-x holidays or list-holidays`  : Displays holidays

Press `p` in calendar buffer.
