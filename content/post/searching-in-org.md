+++
title = "Searching in org and agenda mode"
author = ["Akshay Gaikwad"]
date = 2019-02-25
draft = false
+++

List of command for searching in org file and org agenda mode.

1.  `C-c a T`: Search entries with keywords.

    Get a list of items with special keyword.

    `C-c a T`
    `Keyword (or KWD1|K2D2|...): CANCELLED` gives list of CANCELLED tasks

    ```shell
    Global list of TODO items of type: CANCELLED
    Available with ‘N r’: (0)[ALL] (1)TODO (2)STARTED (3)WAITING (4)APPT (5)DONE (6)CANCELLED (7)DEFERRED
      org:        CANCELLED Taks1 Heading
      org:        CANCELLED Taks2 Heading
    ```

2.  `C-c a m`: Search with Tag, Properties

    Match tag, property query.

    `C-c a m`
    `Match: home` list out tasks with :home: tag.

    ```shell
    Headlines with TAGS match: home
    Press ‘C-u r’ to search again with new search string
      Tasks:      TODO Meet old friends                                                           :home:
    ```

    Specifying property like `Match: status="Accepted"` will returns
    tasks which has status property set to Accepted.

    It can be enfold in brackets, which instruct org-mode to treat it
    as a query expression.

    `Match: status={Progress}`

    ```shell
    Headlines with TAGS match: status={Progress}
    Press ‘C-u r’ to search again with new search string
      BOARD:   TODO Write blog on searching in org mode
      BOARD:   TODO Task with heading
    ```

    `C-c a M` is like m, but returns only TODO entries as result.

3.  `C-c a s`: Serach in file with word/regex

    Search for kwyword in org file.

    `C-c a s`
    `Phrase or [+-]Word/{Regexp} ...: work`

    ```shell
    Search words: work
    Press ‘[’, ‘]’ to add/sub word, ‘{’, ‘}’ to add/sub regexp, ‘C-u r’ to edit
      org:        DONE GTD workflow
      org:        GTD link
    ```

    `C-c a S` is similar to `s` but returns only entries.

    For more detailed information about advance searching in org, read
      [advance searching tutorial in org-mode site](https://orgmode.org/worg/org-tutorials/advanced-searching.html).


## Search with scheduled and deadline dates {#search-with-scheduled-and-deadline-dates}

1.  `C-c / d`: Check Org deadline.

    Highlight deadline entries within `org-deadline-warning-days`.&nbsp;[^fn:1]

    `C-7 C-c / d` shows all deadlines due 7 days.

2.  `C-c / b`: Check deadline/schedule before date.

    Check deadline before date inputted.

    `C-c / b` `2019-02-27` displays deadline/schedules tasks before
    2019-02-27 date.

    ```shell
    ​* Tasks [2/10]                                                     :PERSONAL:
      ** TODO Read book
         SCHEDULED: <2019-02-25 Mon>
         :PROPERTIES:...
      ** TODO Typing practice
      SCHEDULED: <2019-02-26 Tue>
      :PROPERTIES:...
    ...
      ** DONE Read blog post
      DEADLINE: <2019-02-22 Fri>
    ```

3.  `C-c / a`: Check deadline/schedule after date.

    Sparse tree for deadlines and schedules items after a given date.

Read [inserting deadline and schedule manual page](https://orgmode.org/manual/Inserting-deadline_002fschedule.html) on orgmode.

[^fn:1]: `org-deadline-warings-days` can use to specify days counts past-due to show waring of deadline in agenda view. `(setq org-deadline-warings-days 3)` shows deadline within 3 days.