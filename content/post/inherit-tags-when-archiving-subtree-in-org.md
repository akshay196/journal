+++
title = "Inherit tags when archiving subtree in Org"
author = ["Akshay Gaikwad"]
date = 2019-04-21
draft = false
+++

`org-archive-subtree-add-inherited-tags` is a variable defined in `org-archive.el`.

This variable appends inherited tags when archiving a subtree. Original value is `infile`.

It is now set to `t` (always) which appends tags from parent of the tree to archived tree.

```lisp
After archiving "Sub Heading":

 * Heading            :headtag:    =>   * Sub heading       :headtag:subtag:
 ** Sub Heading       :subtag:
```

Reference:
   [Mail by Carsten Dominik for implemetation of org-archive-subtree-add-inherited-tags](https://lists.gnu.org/archive/html/emacs-orgmode/2011-02/msg00036.html)